import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

@immutable
class AlwaysDragEnabledScrollBehavior extends MaterialScrollBehavior {
  const AlwaysDragEnabledScrollBehavior();

  @override
  Set<PointerDeviceKind> get dragDevices => PointerDeviceKind.values.toSet();
}
