import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:routemaster/routemaster.dart';

import 'feed_item.dart';
import 'material_layout_body.dart';

@immutable
class FeedItemListView extends StatefulWidget {
  const FeedItemListView({
    required this.url,
    Key? key,
  }) : super(key: key);

  final String url;

  @override
  _FeedItemListViewState createState() => _FeedItemListViewState();
}

class _FeedItemListViewState extends State<FeedItemListView>
    with AutomaticKeepAliveClientMixin {
  late final StreamController<String> _bodyController;

  @override
  bool get wantKeepAlive => true;

  Future<void> _requestUrl() async {
    final response = await Client().get(Uri.parse(widget.url));
    _bodyController.sink.add(response.body);
  }

  @override
  void initState() {
    super.initState();
    _bodyController = StreamController<String>();
    unawaited(_requestUrl());
  }

  @override
  void dispose() {
    _bodyController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return StreamBuilder<String>(
      stream: _bodyController.stream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return const Center(child: CircularProgressIndicator());
        }

        final body = snapshot.data ?? '';
        final items = FeedItem.parse(body);

        return MaterialLayoutBody(
          child: Scrollbar(
            child: CustomScrollView(
              slivers: [
                CupertinoSliverRefreshControl(onRefresh: _requestUrl),
                _Content(items: items),
              ],
            ),
          ),
        );
      },
    );
  }
}

@immutable
class _Content extends StatelessWidget {
  const _Content({
    required this.items,
    Key? key,
  }) : super(key: key);

  final List<FeedItem> items;

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          if (index.isEven) {
            return const Divider(height: 1);
          }

          final item = items[index ~/ 2];
          return ListTile(
            leading: _Thumbnail(item: item),
            title: Text(item.title, softWrap: false),
            subtitle: Text(item.date, softWrap: false),
            onTap: () {
              final url = Uri.encodeComponent(item.url);
              Routemaster.of(context).push('/home/detail/$url');
            },
          );
        },
        childCount: items.length * 2 + 1,
      ),
    );
  }
}

@immutable
class _Thumbnail extends StatelessWidget {
  const _Thumbnail({
    required this.item,
    Key? key,
  }) : super(key: key);

  final FeedItem item;

  @override
  Widget build(BuildContext context) {
    final border = Border.all(color: Theme.of(context).dividerColor);
    const borderRadius = BorderRadius.all(Radius.circular(4));

    return Container(
      width: 48,
      height: 48,
      foregroundDecoration: BoxDecoration(
        border: border,
        borderRadius: borderRadius,
      ),
      child: ClipRRect(
        borderRadius: borderRadius,
        child: item.thumbnail.isNotEmpty
            ? Image.network(
                item.thumbnail,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) {
                  return const Icon(Icons.broken_image);
                },
              )
            : const FlutterLogo(),
      ),
    );
  }
}
