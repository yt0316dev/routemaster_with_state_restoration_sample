import 'package:flutter/material.dart';

@immutable
class MaterialLayoutBody extends StatelessWidget {
  const MaterialLayoutBody({
    this.child,
    Key? key,
  }) : super(key: key);

  final Widget? child;

  // https://material.io/design/layout/responsive-layout-grid.html#breakpoints
  double _calculateWidth(double maxWidth) {
    if (maxWidth >= 1440) {
      return 1040;
    }

    if (maxWidth >= 1240) {
      return maxWidth - 200 * 2;
    }

    if (maxWidth >= 905) {
      return 840;
    }

    if (maxWidth >= 600) {
      return maxWidth - 32 * 2;
    }

    return maxWidth - 16 * 2;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: LayoutBuilder(
        builder: (context, constraints) {
          final width = _calculateWidth(constraints.maxWidth);

          return ConstrainedBox(
            constraints: BoxConstraints.loose(Size.fromWidth(width)),
            child: Material(
              color: Theme.of(context).cardColor,
              elevation: 4,
              child: child,
            ),
          );
        },
      ),
    );
  }
}
