import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_url_launcher/fwfh_url_launcher.dart';

import 'material_layout_body.dart';
import 'root.dart';

const _html = '''
<h1>About this app</h1>
<p>This app is routemaster sample app with state restoration.</p>
<a href="https://gitlab.com/yt0316dev/routemaster_with_state_restoration_sample">
  <p>https://gitlab.com/yt0316dev/routemaster_with_state_restoration_sample</p>
</a>
<hr>
<ul>
  <li>Cupertino style tabs (Home and Info)</li>
  <li>Drawer on root of app</li>
  <li>Observe navigation with custom RoutemasterObserver</li>
  <li>Save and restore current path using RestorationMixin with RestorableString</li>
</ul>
<hr>
<ul>
  <li>Home</li>
  <ul>
    <li>Simple feed reader with material style tab view</li>
    <li>Cupertino style pull-to-refresh</li>
    <li>Feed item detail view with HTML rendering by flutter_widget_from_html_core</li>
  </ul>
</ul>
<hr>
<ul>
  <li>Info</li>
  <ul>
    <li>App info with flutter_widget_from_html_core</li>
  </ul>
</ul>
<hr>
<ul>
  <li>Drawer</li>
  <ul>
    <li>Open drag gesture is enabled by context</li>
    <li>Brightness config</li>
    <li>Route to licenses page</li>
  </ul>
</ul>
<hr>
''';

@immutable
class Info extends StatelessWidget {
  const Info({
    Key? key,
  }) : super(key: key);

  String _getHrColor(Color color) {
    final argbString = color.value.toRadixString(16);
    return '${argbString.substring(2, 8)}${argbString.substring(0, 2)}';
  }

  @override
  Widget build(BuildContext context) {
    final dividerColor = Theme.of(context).dividerColor;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Root.scaffoldKey.currentState?.openDrawer(),
          icon: const Icon(Icons.menu),
        ),
        title: const Text('Info'),
      ),
      body: MaterialLayoutBody(
        child: Scrollbar(
          child: HtmlWidget(
            _html,
            rebuildTriggers: RebuildTriggers(<Color>[dividerColor]),
            renderMode: const ListViewMode(padding: EdgeInsets.all(16)),
            factoryBuilder: _WidgetFactory.new,
            customStylesBuilder: (element) {
              if (element.localName == 'hr') {
                return {
                  'background-color': '#${_getHrColor(dividerColor)}',
                };
              }
            },
          ),
        ),
      ),
    );
  }
}

class _WidgetFactory extends WidgetFactory with UrlLauncherFactory {}
