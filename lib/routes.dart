import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'feed_item_detail_view.dart';
import 'feed_item_list_view.dart';
import 'home.dart';
import 'info.dart';
import 'root.dart';
import 'sites.dart';

final routes = RouteMap(
  routes: {
    '/': (route) {
      return const CupertinoTabPage(
        child: Root(),
        paths: ['/home', '/info'],
      );
    },
    '/home': (route) {
      return TabPage(
        child: const Home(),
        paths: sites.asMap().keys.map((index) => 'tab/$index').toList(),
      );
    },
    '/home/tab/:index': (route) {
      final index = int.tryParse(route.pathParameters['index'] ?? '') ?? 0;
      return MaterialPage<void>(child: FeedItemListView(url: sites[index].url));
    },
    '/home/detail/:url': (route) {
      final url = Uri.decodeComponent(route.pathParameters['url'] ?? '');
      return MaterialPage<void>(child: FeedItemDetailView(url: url));
    },
    '/licenses': (route) {
      return const MaterialPage<void>(child: LicensePage());
    },
    '/info': (route) {
      return const MaterialPage<void>(child: Info());
    },
  },
);
