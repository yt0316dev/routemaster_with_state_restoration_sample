import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app.dart';
import 'brightness_state.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    RootRestorationScope(
      restorationId: 'root',
      child: FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const SizedBox();
          }

          final value = snapshot.data?.getInt('brightness');
          return BrighnessConfig(
            initialValue: value != null ? Brightness.values[value] : null,
            child: const App(),
          );
        },
      ),
    ),
  );
}
