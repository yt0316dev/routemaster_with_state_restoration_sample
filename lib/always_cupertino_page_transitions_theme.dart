import 'package:flutter/material.dart';

@immutable
class AlwaysCupertinoPageTransitionsTheme extends PageTransitionsTheme {
  const AlwaysCupertinoPageTransitionsTheme();

  static const builder = CupertinoPageTransitionsBuilder();

  @override
  Widget buildTransitions<T>(
    PageRoute<T> route,
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return builder.buildTransitions<T>(
      route,
      context,
      animation,
      secondaryAnimation,
      child,
    );
  }
}
