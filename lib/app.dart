import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'always_cupertino_page_transitions_theme.dart';
import 'always_drag_enabled_scroll_behavior.dart';
import 'brightness_state.dart';
import 'routes.dart';

class _Observer extends RoutemasterObserver {
  _Observer(this.callback);

  final void Function(RouteData routeData) callback;

  @override
  void didChangeRoute(RouteData routeData, Page<dynamic> page) {
    callback(routeData);
  }
}

@immutable
class App extends StatefulWidget {
  const App({
    Key? key,
  }) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _RouterDelegate extends RoutemasterDelegate {
  _RouterDelegate({
    required RouteMap Function(BuildContext context) routesBuilder,
    required List<NavigatorObserver> observers,
  }) : super(routesBuilder: routesBuilder, observers: observers);

  @override
  Future<bool> popRoute() => super.pop();
}

class _AppState extends State<App> with RestorationMixin {
  final RestorableString _path = RestorableString('/home');

  @override
  String? get restorationId => '_AppState';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_path, 'path');
  }

  late final _routeInformationProvider = PlatformRouteInformationProvider(
    initialRouteInformation: RouteInformation(location: _path.value),
  );

  late final _routerDelegate = _RouterDelegate(
    routesBuilder: (context) => routes,
    observers: [_Observer((routeData) => _path.value = routeData.fullPath)],
  );

  @override
  void dispose() {
    _path.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: const RoutemasterParser(),
      routeInformationProvider: _routeInformationProvider,
      routerDelegate: _routerDelegate,
      theme: ThemeData(
        brightness: BrighnessConfig.of(context).value,
        pageTransitionsTheme: const AlwaysCupertinoPageTransitionsTheme(),
        primarySwatch: Colors.blueGrey,
        platform: TargetPlatform.iOS,
        visualDensity: VisualDensity.standard,
      ),
      scrollBehavior: const AlwaysDragEnabledScrollBehavior(),
    );
  }
}
