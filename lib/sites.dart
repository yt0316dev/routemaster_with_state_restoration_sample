import 'package:flutter/foundation.dart';

@immutable
class _Site {
  const _Site(this.title, this.url);
  final String title;
  final String url;
}

const sites = [
  _Site(
    'pub.dev',
    'https://pub.dev/feed.atom',
  ),
  _Site(
    'flutter/flutter',
    'https://github.com/flutter/flutter/commits/master.atom',
  ),
  _Site(
    'tomgilder/routemaster',
    'https://github.com/tomgilder/routemaster/commits/main.atom',
  ),
  _Site(
    'dart-lang/sdk',
    'https://github.com/dart-lang/sdk/commits/main.atom',
  ),
  _Site(
    'dart-lang/linter',
    'https://github.com/dart-lang/linter/commits/master.atom',
  ),
  _Site(
    'sudame/dart-rss',
    'https://github.com/sudame/dart-rss/commits/master.atom',
  ),
  _Site(
    'daohoangson/flutter_widget_from_html/core',
    'https://github.com/daohoangson/flutter_widget_from_html/commits/master/packages/core.atom',
  ),
  _Site(
    'daohoangson/flutter_widget_from_html/fwfh_url_launcher',
    'https://github.com/daohoangson/flutter_widget_from_html/commits/master/packages/fwfh_url_launcher.atom',
  ),
];
