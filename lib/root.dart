import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'root_drawer.dart';
import 'tab_switching_view.dart';

@immutable
class Root extends StatelessWidget {
  const Root({
    Key? key,
  }) : super(key: key);

  static final scaffoldKey = GlobalKey<ScaffoldState>();

  bool isDrawerEnableOpenDragGesture(String path) {
    return !path.startsWith('/home/detail');
  }

  @override
  Widget build(BuildContext context) {
    final tabState = CupertinoTabPage.of(context);
    final controller = tabState.controller;

    return Scaffold(
      key: scaffoldKey,
      body: TabSwitchingView(
        currentTabIndex: controller.index,
        tabCount: tabState.stacks.length,
        tabBuilder: tabState.tabBuilder,
      ),
      drawer: const RootDrawer(),
      drawerEnableOpenDragGesture: isDrawerEnableOpenDragGesture(
        Routemaster.of(context).currentRoute.fullPath,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: controller.index,
        onTap: (index) => controller.index = index,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.info),
            label: 'Info',
          ),
        ],
      ),
    );
  }
}
