import 'package:dart_rss/dart_rss.dart';
import 'package:dart_rss/domain/rss1_feed.dart';
import 'package:flutter/material.dart';

extension _IterableExtension<T> on Iterable<T> {
  T? elementOrNullAt(int index) {
    try {
      return elementAt(index);
      // ignore: avoid_catches_without_on_clauses
    } catch (_) {
      return null;
    }
  }
}

@immutable
class FeedItem {
  const FeedItem({
    required this.title,
    required this.url,
    required this.date,
    required this.thumbnail,
  });

  final String title;
  final String url;
  final String date;
  final String thumbnail;

  static List<FeedItem> parse(String xmlString) {
    try {
      final atomFeed = AtomFeed.parse(xmlString);
      return atomFeed.items
          .map(
            (item) => FeedItem(
              title: (item.title ?? '').trim(),
              url: (item.links.elementOrNullAt(0)?.href ?? '').trim(),
              date: item.updated ?? item.published ?? '',
              thumbnail: item.media?.thumbnails.elementOrNullAt(0)?.url ?? '',
            ),
          )
          .toList();
    } catch (_) {} // ignore: avoid_catches_without_on_clauses

    try {
      final rssFeed = RssFeed.parse(xmlString);
      return rssFeed.items
          .map(
            (item) => FeedItem(
              title: (item.title ?? '').trim(),
              url: (item.link ?? '').trim(),
              date: item.pubDate ?? '',
              thumbnail: item.media?.thumbnails.elementOrNullAt(0)?.url ??
                  item.itunes?.image?.href ??
                  '',
            ),
          )
          .toList();
    } catch (_) {} // ignore: avoid_catches_without_on_clauses

    try {
      final rss1Feed = Rss1Feed.parse(xmlString);
      return rss1Feed.items
          .map(
            (item) => FeedItem(
              title: (item.title ?? '').trim(),
              url: (item.link ?? '').trim(),
              date: item.dc?.date ?? '',
              thumbnail: item.content?.images.elementOrNullAt(0) ?? '',
            ),
          )
          .toList();
    } catch (_) {} // ignore: avoid_catches_without_on_clauses

    throw Error();
  }
}
