import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'always_fade_upwards_page_transitions_theme.dart';
import 'root.dart';
import 'sites.dart';

@immutable
class Home extends StatelessWidget {
  const Home({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final tabPage = TabPage.of(context);
    final controller = tabPage.controller;

    final themeData = Theme.of(context).copyWith(
      pageTransitionsTheme: const AlwaysFadeUpwardsPageTransitionsTheme(),
    );

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Root.scaffoldKey.currentState?.openDrawer(),
          icon: const Icon(Icons.menu),
        ),
        title: const Text('Home'),
        bottom: TabBar(
          controller: controller,
          isScrollable: true,
          tabs: sites.map((site) {
            return Tab(text: site.title);
          }).toList(),
        ),
      ),
      body: Theme(
        data: themeData,
        child: TabBarView(
          controller: controller,
          children: tabPage.stacks.map((stack) {
            return PageStackNavigator(stack: stack);
          }).toList(),
        ),
      ),
    );
  }
}
