import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BrightnessValueNotifier extends ValueNotifier<Brightness?> {
  BrightnessValueNotifier(Brightness? value) : super(value);

  @override
  set value(Brightness? newValue) {
    super.value = newValue;
    SharedPreferences.getInstance().then((prefs) {
      if (newValue != null) {
        prefs.setInt('brightness', newValue.index);
      } else {
        prefs.remove('brightness');
      }
    });
  }
}

@immutable
class _Notifier extends InheritedNotifier<BrightnessValueNotifier> {
  const _Notifier({
    required BrightnessValueNotifier notifier,
    required Widget child,
    Key? key,
  }) : super(key: key, notifier: notifier, child: child);
}

@immutable
class BrighnessConfig extends StatefulWidget {
  const BrighnessConfig({
    required this.child,
    this.initialValue,
    Key? key,
  }) : super(key: key);

  final Widget child;
  final Brightness? initialValue;

  static BrightnessValueNotifier of(BuildContext context) {
    final notifier =
        context.dependOnInheritedWidgetOfExactType<_Notifier>()?.notifier;
    if (notifier == null) {
      throw Error();
    }

    return notifier;
  }

  @override
  _BrighnessConfigState createState() => _BrighnessConfigState();
}

class _BrighnessConfigState extends State<BrighnessConfig> {
  late final _brightness = BrightnessValueNotifier(widget.initialValue);

  @override
  void dispose() {
    _brightness.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _Notifier(notifier: _brightness, child: widget.child);
  }
}
