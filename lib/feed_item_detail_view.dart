import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_url_launcher/fwfh_url_launcher.dart';
import 'package:html/dom.dart' as dom;
import 'package:http/http.dart';

import 'material_layout_body.dart';

@immutable
class FeedItemDetailView extends StatelessWidget {
  const FeedItemDetailView({
    required this.url,
    Key? key,
  }) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(url.split('').join('\u200b'), softWrap: false),
      ),
      body: FutureBuilder<Response>(
        future: Client().get(Uri.parse(url)),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const Center(child: CircularProgressIndicator());
          }

          return _Content(url: url, html: snapshot.data?.body ?? '');
        },
      ),
    );
  }
}

@immutable
class _Content extends StatelessWidget {
  const _Content({
    required this.url,
    required this.html,
    Key? key,
  }) : super(key: key);

  final String url;
  final String html;

  Widget? _buildTableElement(dom.Element element) {
    if (element.localName == 'table') {
      final lines = const LineSplitter()
          .convert(element.text)
          .where((line) => line.trim().isNotEmpty);

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: lines.map((line) {
          return Text(line, maxLines: 1);
        }).toList(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialLayoutBody(
      child: Scrollbar(
        child: HtmlWidget(
          html,
          renderMode: const ListViewMode(padding: EdgeInsets.all(16)),
          factoryBuilder: () => _WidgetFactory(url),
          onLoadingBuilder: (context, element, loadingProgress) {
            return const SizedBox();
          },
          customWidgetBuilder: _buildTableElement,
        ),
      ),
    );
  }
}

class _WidgetFactory extends WidgetFactory with UrlLauncherFactory {
  _WidgetFactory(this.baseUrl);

  final String baseUrl;

  @override
  Future<bool> onTapUrl(String url) async {
    return super.onTapUrl(Uri.parse(baseUrl).resolve(url).toString());
  }
}
