import 'package:flutter/material.dart';

@immutable
class AlwaysFadeUpwardsPageTransitionsTheme extends PageTransitionsTheme {
  const AlwaysFadeUpwardsPageTransitionsTheme();

  static const builder = FadeUpwardsPageTransitionsBuilder();

  @override
  Widget buildTransitions<T>(
    PageRoute<T> route,
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return builder.buildTransitions<T>(
      route,
      context,
      animation,
      secondaryAnimation,
      child,
    );
  }
}
