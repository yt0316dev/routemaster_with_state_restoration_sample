import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'brightness_state.dart';

@immutable
class RootDrawer extends StatelessWidget {
  const RootDrawer({
    Key? key,
  }) : super(key: key);

  static final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    final brighnessConfig = BrighnessConfig.of(context);

    return Drawer(
      child: ListView(
        controller: _controller,
        children: [
          const DrawerHeader(
            child: Text('RootDrawer'),
          ),
          const ListTile(title: Text('Brightness config')),
          RadioListTile<Brightness?>(
            dense: true,
            title: const Text('Default'),
            value: null,
            groupValue: brighnessConfig.value,
            onChanged: (value) => brighnessConfig.value = value,
          ),
          RadioListTile<Brightness?>(
            dense: true,
            title: const Text('Force Light'),
            value: Brightness.light,
            groupValue: brighnessConfig.value,
            onChanged: (value) => brighnessConfig.value = value,
          ),
          RadioListTile<Brightness?>(
            dense: true,
            title: const Text('Force Dark'),
            value: Brightness.dark,
            groupValue: brighnessConfig.value,
            onChanged: (value) => brighnessConfig.value = value,
          ),
          const Divider(),
          ListTile(
            title: const Text('Licenses...'),
            onTap: () {
              Navigator.of(context).pop();
              Routemaster.of(context).push('/licenses');
            },
          ),
          const Divider(),
        ],
      ),
    );
  }
}
